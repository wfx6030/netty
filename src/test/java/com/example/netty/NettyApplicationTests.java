package com.example.netty;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.netty.client.tcp.TcpClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class NettyApplicationTests {

    private final static Logger logger = LoggerFactory.getLogger(NettyApplicationTests.class);


    public static void main(String[] args) throws Exception {

        //故障树
        String msg1 = "{\"antenna_id\":\"ANT04\",\"data_type\":\"ErrorTreeData\",\"error_tree\":{\"children\":[{\"children\":[{\"children\":[{\"name\":\"ADU通信\",\"state\":\"异常\",\"state_describe\":\"驱动器通信链路异常,建议排查相关硬件连接。\"},{\"name\":\"ADU通信\",\"state\":\"异常\",\"state_describe\":\"极化通信链路状态正常。\"}],\"name\":\"通信状态\",\"state\":\"异常\",\"state_describe\":\"\"},{\"children\":[{\"name\":\"方位跳码\",\"state\":\"正常\",\"state_describe\":\"方位角度编码状态正常。\"},{\"name\":\"俯仰跳码\",\"state\":\"正常\",\"state_describe\":\"俯仰角度编码状态正常。\"}],\"name\":\"跳码状态\",\"state\":\"正常\",\"state_describe\":\"跳码节点状态正常。\"},{\"children\":[{\"name\":\"方位限位\",\"state\":\"异常\",\"state_describe\":\"方位限位状态异常,建议查看该轴运动范围是否超限。\"},{\"name\":\"俯仰限位\",\"state\":\"异常\",\"state_describe\":\"俯仰限位状态异常,建议查看该轴运动范围是否超限。\"}],\"name\":\"限位状态\",\"state\":\"异常\",\"state_describe\":\"限位节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"name\":\"方位过速\",\"state\":\"正常\",\"state_describe\":\"方位运行速度状态正常。\"},{\"name\":\"俯仰过速\",\"state\":\"正常\",\"state_describe\":\"方位运行速度状态正常。\"}],\"name\":\"过速状态\",\"state\":\"正常\",\"state_describe\":\"过速节点状态正常。\"},{\"children\":[{\"name\":\"方位控制链路\",\"state\":\"异常\",\"state_describe\":\"\"},{\"name\":\"俯仰控制链路\",\"state\":\"异常\",\"state_describe\":\"伺服俯仰控制链路故障,请检查伺服器。\"}],\"name\":\"伺服控制链路状态\",\"state\":\"异常\",\"state_describe\":\"\"},{\"children\":[{\"name\":\"方位1#功放状态\",\"state\":\"异常\",\"state_describe\":\"方位功放1#故障,请检查伺服器。\"},{\"name\":\"方位2#功放状态\",\"state\":\"异常\",\"state_describe\":\"方位功放2#故障,请检查伺服器。\"},{\"name\":\"俯仰1#功放状态\",\"state\":\"异常\",\"state_describe\":\"俯仰功放1#故障,请检查伺服器。\"},{\"name\":\"俯仰2#功放状态\",\"state\":\"异常\",\"state_describe\":\"俯仰功放2#故障,请检查伺服器。\"}],\"name\":\"伺服功放状态\",\"state\":\"异常\",\"state_describe\":\"伺服功放状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"name\":\"供电A相\",\"state\":\"异常\",\"state_describe\":\"\"},{\"name\":\"供电B相\",\"state\":\"异常\",\"state_describe\":\"伺服供电B相故障,请检查伺服器。\"},{\"name\":\"供电C相\",\"state_describe\":\"伺服供电C相故障,请检查伺服器。\"}],\"name\":\"伺服供电状态\",\"state\":\"异常\",\"state_describe\":\"伺服供电节点状态异常,点击叶节点获取更多详细信息与建议。\"}],\"name\":\"伺服系统\",\"state\":\"异常\",\"state_describe\":\"\"},{\"children\":[{\"children\":[{\"name\":\"方位1#电流\",\"state\":\"异常\",\"state_describe\":\"方位电机1#电流过流,请检查伺服器。\"},{\"name\":\"方位2#电流\",\"state\":\"异常\",\"state_describe\":\"方位电机2#电流过流,请检查伺服器。\"},{\"name\":\"俯仰1#电流\",\"state\":\"异常\",\"state_describe\":\"俯仰电机1#电流过流,请检查伺服器。\"},{\"name\":\"俯仰2#电流\",\"state\":\"异常\",\"state_describe\":\"俯仰电机1#电流过流,请检查伺服器。\"}],\"name\":\"电流状态\",\"state\":\"异常\",\"state_describe\":\"电机电流状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"name\":\"方位1#制动器\",\"state\":\"异常\",\"state_describe\":\"方位制动器1#故障,请检查伺服器。\"},{\"name\":\"方位1#制动器\",\"state\":\"异常\",\"state_describe\":\"方位制动器2#故障,请检查伺服器。\"},{\"name\":\"俯仰1#制动器\",\"state\":\"异常\",\"state_describe\":\"俯仰制动器1#故障,请检查伺服器。\"},{\"name\":\"俯仰2#制动器\",\"state\":\"异常\",\"state_describe\":\"俯仰制动器2#故障,请检查伺服器。\"}],\"name\":\"制动器状态\",\"state\":\"异常\",\"state_describe\":\"制动器状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"name\":\"方位机械传动\",\"state\":\"异常\",\"state_describe\":\"方位机械传动故障,请检查伺服器。\"},{\"name\":\"方位机械传动\",\"state\":\"异常\",\"state_describe\":\"俯仰机械传动故障,请检查伺服器。\"}],\"name\":\"机械传动总状态\",\"state\":\"异常\",\"state_describe\":\"机械传动状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"name\":\"方位1#电机温度\",\"state\":\"正常\",\"state_describe\":\"方位电机1#温度状态正常。\"},{\"name\":\"方位2#电机温度\",\"state\":\"正常\",\"state_describe\":\"方位电机2#温度状态正常。\"},{\"name\":\"俯仰1#电机温度\",\"state\":\"正常\",\"state_describe\":\"俯仰电机1#温度状态正常。\"},{\"name\":\"俯仰2#电机温度\",\"state\":\"正常\",\"state_describe\":\"俯仰电机2#温度状态正常。\"}],\"name\":\"电机温度\",\"state\":\"正常\",\"state_describe\":\"电机温度状态节点状态正常。\"},{\"children\":[{\"name\":\"方位1#电机振动\",\"state\":\"正常\",\"state_describe\":\"方位电机1#振动状态正常。\"},{\"name\":\"方位2#电机振动\",\"state\":\"正常\",\"state_describe\":\"方位电机2#振动状态正常。\"},{\"name\":\"俯仰1#电机振动\",\"state\":\"正常\",\"state_describe\":\"俯仰电机1#振动状态正常。\"},{\"name\":\"俯仰2#电机振动\",\"state\":\"正常\",\"state_describe\":\"俯仰电机2#振动状态正常。\"},{\"name\":\"方位1#减速箱振动\",\"state\":\"正常\",\"state_describe\":\"方位减速箱1#振动状态正常。\"},{\"name\":\"方位2#减速箱振动\",\"state\":\"正常\",\"state_describe\":\"方位减速箱2#振动状态正常。\"},{\"name\":\"俯仰1#减速箱振动\",\"state\":\"正常\",\"state_describe\":\"俯仰减速箱1#振动状态正常。\"},{\"name\":\"俯仰2#减速箱振动\",\"state\":\"正常\",\"state_describe\":\"俯仰减速箱2#振动状态正常。\"}],\"name\":\"电机振动\",\"state\":\"正常\",\"state_describe\":\"振动状态节点状态正常。\"}],\"name\":\"传动系统\",\"state\":\"异常\"},{\"children\":[{\"name\":\"电平状态\",\"state\":\"异常\",\"state_describe\":\"频段#1AGC状态异常,建议确认信道链路以及天线指向是否正常。\"},{\"name\":\"旋向\",\"state\":\"正常\",\"state_describe\":\"频段#1旋向正常。\"}],\"name\":\"信道链路\",\"state\":\"异常\",\"state_describe\":\"信道状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"children\":[{\"children\":[{\"name\":\"中心体温度\",\"state\":\"正常\",\"state_describe\":\"中心体环境温度正常。\"},{\"name\":\"塔基温度\",\"state\":\"正常\",\"state_describe\":\"塔基环境温度正常。\"},{\"name\":\"方位舱温度\",\"state\":\"缺省\",\"state_describe\":\"方位舱环境温度状态异常,建议确认该舱体中各设备运行是否正常。\"},{\"name\":\"俯仰舱温度\",\"state\":\"缺省\",\"state_describe\":\"俯仰舱环境温度状态异常,建议确认该舱体中各设备运行是否正常。\"}],\"name\":\"环境温度\",\"state\":\"正常\",\"state_describe\":\"环境温度总状态节点状态正常。\"},{\"children\":[{\"name\":\"中心体湿度\",\"state\":\"正常\",\"state_describe\":\"中心体环境湿度正常。\"},{\"name\":\"塔基湿度\",\"state\":\"正常\",\"state_describe\":\"塔基环境湿度正常。\"},{\"name\":\"方位舱湿度\",\"state\":\"缺省\",\"state_describe\":\"方位舱环境湿度状态异常,建议确认该舱体中各设备运行是否正常。\"},{\"name\":\"俯仰舱湿度\",\"state\":\"缺省\",\"state_describe\":\"俯仰舱环境湿度状态异常,建议确认该舱体中各设备运行是否正常。\"}],\"name\":\"环境湿度\",\"state\":\"正常\",\"state_describe\":\"环境湿度总状态节点状态正常。\"},{\"children\":[{\"name\":\"塔基水浸状态\",\"state\":\"异常\",\"state_describe\":\"塔基浸水状态，请检查相应位置。\"}],\"name\":\"环境水浸状态\",\"state\":\"异常\",\"state_describe\":\"水浸总状态节点状态异常,点击叶节点获取更多详细信息与建议。\"},{\"name\":\"塔基沉降状态\",\"state\":\"正常\",\"state_describe\":\"塔基水平状态,状态正常。\"}],\"name\":\"环境状态\",\"state\":\"异常\",\"state_describe\":\"环境总状态节点状态异常,点击叶节点获取更多详细信息与建议。\"}],\"state\":\"异常\",\"state_describe\":\"天线总状态节点状态异常,点击叶节点获取更多详细信息与建议。\"}}\t";

        //实时工况
        String msg2 = "{\n" +
                "    \"antenna_id\": \"ANT04\",\n" +
                "    \"az_cabin\": [\n" +
                "        {\n" +
                "            \"name\": \"方位舱温度\",\n" +
                "            \"state\": \"缺省\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位舱湿度\",\n" +
                "            \"state\": \"缺省\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"az_driver1\": [\n" +
                "        {\n" +
                "            \"name\": \"方位1号电机温度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位1号电机振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位1号减速箱振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位1号反馈电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位1号电机电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"az_driver2\": [\n" +
                "        {\n" +
                "            \"name\": \"方位2号电机温度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位2号电机振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位2号减速箱振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位2号反馈电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位2号电机电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"data_type\": \"RealTimeData\",\n" +
                "    \"el_cabin\": [\n" +
                "        {\n" +
                "            \"name\": \"俯仰舱温度\",\n" +
                "            \"state\": \"缺省\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰舱湿度\",\n" +
                "            \"state\": \"缺省\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"el_driver1\": [\n" +
                "        {\n" +
                "            \"name\": \"俯仰1号电机温度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰1号电机振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰1号减速箱振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰1号反馈电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰1号电机电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"el_driver2\": [\n" +
                "        {\n" +
                "            \"name\": \"俯仰2号电机温度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰2号电机振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰2号减速箱振动\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰2号反馈电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰2号电机电压\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"hub_cabin\": [\n" +
                "        {\n" +
                "            \"name\": \"中心体温度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"中心体湿度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"中心体浸水\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"浸水\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"servo\": [\n" +
                "        {\n" +
                "            \"name\": \"方位角度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰角度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位命令角度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰命令角度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"信号电平\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位角速度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰角速度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.000\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位工作方式\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"系统未就绪\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰工作方式\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"系统未就绪\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"伺服控制方式\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"本控\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位逆限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位逆预限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位逆软限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位顺限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位顺预限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"方位顺软限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰上限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰上预限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰上软限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰下限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰下预限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"俯仰下软限位\",\n" +
                "            \"state\": \"限位\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"伺服供电A相\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"伺服供电B相\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"伺服供电C相\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"state\": \"正常\",\n" +
                "    \"time\": \"2022-03-08 13:46:02\",\n" +
                "    \"tower_cabin\": [\n" +
                "        {\n" +
                "            \"name\": \"塔基温度\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"塔基湿度\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"塔基水平度1\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"塔基水平度2\",\n" +
                "            \"state\": \"正常\",\n" +
                "            \"value\": \"0.0\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"塔基浸水\",\n" +
                "            \"state\": \"异常\",\n" +
                "            \"value\": \"浸水\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"value\": \"0.0\"\n" +
                "}";


        //告警
        String msg3 = "{\n" +
                "    \"antenna_id\": \"SH_ANT01\",\n" +
                "    \"antenna_name\": \"1#13米\",\n" +
                "    \"data_type\": \"ErrorLog\",\n" +
                "    \"log_content\": \"天线故障\",\n" +
                "    \"log_time\": \"\",\n" +
                "    \"log_type\": 1\n" +
                "}\r";


        //气象信息
        String msg4 = "{\n" +
                "    \"base_atmos\": 120,\n" +
                "    \"base_humidity\": 50,\n" +
                "    \"base_id\": 1,\n" +
                "    \"base_temperature\": 10,\n" +
                "    \"base_wind_direction\": 20,\n" +
                "    \"base_wind_speed\": 3,\n" +
                "    \"data_type\": \"Meteorology\",\n" +
                "    \"rainNumber\": 0\n" +
                "}";

        //健康评估
        String msg5 = "{\n" +
                "    \"antenna_id\": \"SH_ANT01\",\n" +
                "    \"az_cabin_grade\": \"75\",\n" +
                "    \"az_driver_grade\": \"83\",\n" +
                "    \"center_cabin_grade\": \"90\",\n" +
                "    \"data_type\": \"RealAssessment\",\n" +
                "    \"el_cabin_grade\": \"78\",\n" +
                "    \"el_driver_grade\": \"80\",\n" +
                "    \"servo_grade\": \"88\",\n" +
                "    \"time\": \"2022-03-03 22:13:19\",\n" +
                "    \"total_grade\": \"86\",\n" +
                "    \"tower_cabin_grade\": \"79\"\n" +
                "}\n";

        String msg6 = "{\n" +
                "    \"antenna_id\": \"SH_ANT01\",\n" +
                "    \"data_type\": \"Forecast\",\n" +
                "    \"forecast_array\": [\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-03 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-04 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-05 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-06 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-07 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-08 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-09 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        },\n" +
                "        {\n" +
                "            \"az_cabin_grade\": 99,\n" +
                "            \"az_driver_grade\": 90,\n" +
                "            \"center_cabin_grade\": 80,\n" +
                "            \"el_cabin_grade\": 55,\n" +
                "            \"el_driver_grade\": 70,\n" +
                "            \"servo_grade\": 78,\n" +
                "            \"time\": \"2022-03-10 22:13:19\",\n" +
                "            \"total_grade\": 88,\n" +
                "            \"tower_cabin_grade\": 77\n" +
                "        }\n" +
                "    ]\n" +
                "}";


        String s = JSON.parseObject(msg3).toString();
        System.out.println(s);
        TcpClient tcpClient = new TcpClient("127.0.0.1", 4000);
        for (int i = 0; i < 10; i++) {
            String response = tcpClient.send(JSON.parseObject(msg6).toString() + "\t", 1000);
            System.out.println(response);
        }
    }


}
