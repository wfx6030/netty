package com.example.excutor;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {


    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        String url = "http://192.168.0.13:9090/login.jsp?username=admin&password&=123456";
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();      //map里面是请求体的内容
        map.add("username", "admin");
        map.add("password", "123456");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity("http://192.168.0.13:9090/login.jsp", request, String.class);       //地址替换为自己的
        System.out.println(response.getHeaders().get("Set-Cookie").get(0));

        List<String> cookies = response.getHeaders().get("Set-Cookie");
        String csrf = "";
        for (String cookie : cookies) {
            if (cookie.contains("csrf")) {
                String[] split = cookie.split(";");
                for (String s : split) {
                    if(s.contains("csrf")){
                        csrf = s.split("csrf=")[1];
                    }
                }
            }
        }

        map.clear();
        //在 header 中存入cookies
        headers.put(HttpHeaders.COOKIE, cookies);        //将cookie存入头部
        map.add("csrf", csrf);
        map.add("group", "济南");        //应用名称
        map.add("listPagerPageSize", "25");      //执行器名称
        map.add("listPagerCurrentPage", "1");          //排序方式
        map.add("group", "济南");        //注册方式 ：  0为
        map.add("enableRosterGroups", "true");
        map.add("groupDisplayName", "济南");
        map.add("showGroup", "everybody");
        map.add("updateContactListSettings", "保存共享设置");
        HttpEntity<MultiValueMap<String, String>> request1 = new HttpEntity<>(map, headers);
        ResponseEntity<String> response1 = restTemplate.postForEntity("http://192.168.0.13:9090/group-edit.jsp", request1, String.class);
        System.out.println(response1.getBody());


//localhost:9090/group-edit.jsp?csrf=8WZkl3ceIbqEmMk&group=%E6%B5%8E%E5%8D%97&listPagerPageSize=25&listPagerCurrentPage=1&group=%E6%B5%8E%E5%8D%97&enableRosterGroups=true&groupDisplayName=%E6%B5%8E%E5%8D%97&showGroup=onlyGroup&updateContactListSettings=%E4%BF%9D%E5%AD%98%E5%85%B1%E4%BA%AB%E8%AE%BE%E7%BD%AE


    }
}
