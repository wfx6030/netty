package com.example.netty.service;

import io.netty.channel.ChannelHandlerContext;

public interface CallBack {

    default void response(ChannelHandlerContext ctx, Object msg) throws Exception {
    }

}
