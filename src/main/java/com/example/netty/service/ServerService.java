package com.example.netty.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

public interface ServerService {
    void creat() throws UnknownHostException;

    void downLoad(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException;
}
