package com.example.netty.model;

public enum CoderEnum implements CoderStrEnum {

    ASCII("ASCII"),
    UTF8("UTF-8"),
    GBK("GBK"),
    HEX("HEX");

    private final String code;

    CoderEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }
}
