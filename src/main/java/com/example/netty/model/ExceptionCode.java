package com.example.netty.model;

/**
 * @Author:liyang
 * @Description:系统异常代码表
 * @Date: created in 2018/9/27
 * @Modified By:
 */
public enum ExceptionCode {

    /**
     * 系统未知错误
     */
    SYSTEM_ERROR(500, "网络异常，请稍后再试！"),

    /**
     * 数据库操作异常 700+
     */
    SQL_QUERY_EXCEPTION(700, "SQL查询异常！"),
    SQL_INSET_EXCEPTION(701, "SQL插入异常！"),
    SQL_UPDATE_EXCEPTION(702, "SQL更新异常！"),
    SQL_DELETE_EXCEPTION(703, "SQL删除异常！"),

    /**
     * 业务操作类异常 800+
     */
    PARAM_EXCEPTION(801, "参数异常！"),
    PARAM_EQUNLINE(802, "设备不在线！"),
    PARAM_EQBUSY(803, "设备忙碌"),
    PARAM_NO_STATIC_PARAM(804, "未获取到设备静态参数"),
    TASKNUM_IS_NULL(805, "任务序列号为空！"),
    TASK_ISSUED_FAILED(806, "任务下发失败，请稍后再试"),
    TASKMAIN_IS_NULL(807, "任务信息为空！"),
    USERID_IS_NULL(808, "用户ID为空！"),
    TEMPLATE_NAME_IS_NULL(809, "频谱模板名称为空！"),
    SPECTRUM_TYPE_IS_NULL(810, "谱图类型为空！"),
    SPECTRUM_IS_NULL(811, "谱图模板内容为空！"),
    SWITCH_IS_EXCEPTION(812, "开关状态码错误！"),
    SAVE_DATA_EXCEPTION(813, "该用户没有保存数据权限！"),
    HDFS_IP_EXCEPTION(814, "文件系统路径为空，无法正常存储数据，请联系管理员！"),
    STATION_IP_NULL(815, "无法找到站点信息！"),
    TASK_CACHE_IS_NULL(816, "无法找到缓存任务信息！"),
    STOP_TASK_FAILED(817, "任务停止失败"),
    TASK_DATA_IS_NULL(818, "该任务没有历史数据存在！"),
    TASK_TYPE_NOT_FOUND(819, "未知的任务类型"),
    DATA_NUMBER_IS_NULL(820, "数据条目必须大于等于1！"),
    MAX_PAGE_SIZE(821, "超出系统最大分页数！"),
    EQU_STOP(822, "设备已停用"),
    MAX_WINDOW_NUM_EXCEPTION(823, "当前系统最大支持4个窗口!"),
    TASK_STASTISTIC_IS_NULL(824, "该任务没有统计数据存在！"),
    SERIALIZE_DATA_IS_EXCEPTION(825, "数据格式有误，请稍后再试！"),
    TASK_MAX_CHANNEL_EXCEPTION(826, "超出报表条数最大支持数，报表最大支持20000个信道数据导出！"),
    TASK_ZERO_CHANNEL_EXCEPTION(827, "任务无统计数据！"),
    CENTER_EXCEPTION(828, "Nova中心的任务，无法查看数据！"),
    MONTH_REPORT_EXPORT_EXCEPTION(829, "月报导出失败，请重试！"),
    SERVICE_EXCEPTION(830, "服务调用失败，请重试！"),

    /**
     * 网络通信异常 900+
     */
    CONNECTION_NUMBER_EXCEPTION(900, "超出最大连接数！"),
    RESPONSE_TIMEOUT(901, "响应超时，请稍后再试！"),
    CONNECTION_EXCEPTION(902, "SOCKET连接异常！"),
    SOCKET_CHANNEL_EXCEPTION(903, "SOCKET数据处理异常！"),
    CREATE_SNMP_EXCEPTION(904, "创建SNMP异常！"),

    /**
     * 字符串转化异常
     */
    JSON_SYNTAX_EXCEPTION(1000, "JSON字符串转换异常！"),
    CODING_SYNTAX_EXCEPTION(1001, "字符串编码转化异常！"),
    CORN_SYNTAX_EXCEPTION(1002, "周期表达式转化异常！"),

    /**
     * 超过最大字符串
     **/
    STRING_MAX_EXCEPTION(1500, "超过最大字符串！"),

    /**
     * 文件操作类异常
     **/
    FILE_DELETE_EXCEPTION(2000, "文件删除异常！"),
    FILE_UPLOAD_EXCEPTION(2001, "文件上传异常！"),

    /**
     * HDFS接口数据异常
     **/
    HDFS_GET_EXCEPTION(2500, "查看HDFS文件系统是否正常运行！"),

    /**
     * 时间转换异常
     **/
    TIME_TRANS_EXCEPTION(2700, "时间转换异常！");

    ExceptionCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
