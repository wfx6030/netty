package com.example.netty.model;

import java.util.Arrays;
import java.util.Objects;

public interface CoderStrEnum {


     static <E extends Enum<?> & CoderStrEnum> E codeOf(String code, Class<E> enumClazz) {
        return Arrays.stream(enumClazz.getEnumConstants()).filter(x -> Objects.equals(x.getCode(), code)).findAny().get();
    }

     String getCode();
}
