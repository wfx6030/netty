package com.example.netty.model;

import java.io.Serializable;

/**
 * @Author:liyang
 * @Description: 数据返回统一格式
 * @Date: created in 2017/12/15
 * @Modified By:
 */
public class R<T> implements Serializable {

    private int code;
    private String msg;
    private T body;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public R<T> put(int code, String msg, T body) {
        this.setCode(code);
        this.setMsg(msg);
        this.setBody(body);
        return this;
    }

    public R exception(int code, String msg) {
        this.setCode(code);
        this.setMsg(msg);
        return this;
    }

    public R exception() {
        this.setCode(ExceptionCode.SYSTEM_ERROR.getCode());
        this.setMsg(ExceptionCode.SYSTEM_ERROR.getMessage());
        return this;
    }

    public R<T> ok() {
        this.setCode(0);
        return this;
    }

    public R<T> putBody(T body) {
        this.setBody(body);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", body=" + body +
                '}';
    }
}
