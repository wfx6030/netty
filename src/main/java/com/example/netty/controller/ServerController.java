package com.example.netty.controller;


import com.example.netty.model.R;
import com.example.netty.service.ServerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

@RestController
@RequestMapping("api/netty/service")
public class ServerController {

    private final static Logger logger = LoggerFactory.getLogger(ServerController.class);

    @Autowired
    private ServerService serverService;

    @PostMapping("creat")
    public R creat() throws UnknownHostException {
        serverService.creat();
        return new R().ok();
    }

    @GetMapping("/downLoad")
    public void downLoad(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        serverService.downLoad(request,response);
    }


}
