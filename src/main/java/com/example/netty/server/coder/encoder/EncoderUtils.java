package com.example.netty.server.coder.encoder;

import com.example.netty.model.CoderEnum;
import com.example.netty.model.CoderStrEnum;
import com.example.netty.server.coder.encoder.plugns.HexEncoder;
import com.sun.istack.internal.NotNull;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import java.nio.charset.Charset;

public class EncoderUtils {

    public static ChannelHandler getInstance(@NotNull String coderType) {
        switch (CoderStrEnum.codeOf(coderType, CoderEnum.class)) {
            case GBK:
                return new StringEncoder(Charset.forName("GBK"));
            case ASCII:
                return new StringEncoder(CharsetUtil.US_ASCII);
            case HEX:
                return new HexEncoder();
            default:
                return new StringEncoder(CharsetUtil.UTF_8);
        }
    }
}
