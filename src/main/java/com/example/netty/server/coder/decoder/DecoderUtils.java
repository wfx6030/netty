package com.example.netty.server.coder.decoder;

import com.example.netty.model.CoderEnum;
import com.example.netty.model.CoderStrEnum;
import com.example.netty.server.coder.decoder.plugns.HexDecoder;
import com.sun.istack.internal.NotNull;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.util.CharsetUtil;

import java.nio.charset.Charset;

public class DecoderUtils {

    public static ChannelHandler getInstance(@NotNull String coderType) {
        switch (CoderStrEnum.codeOf(coderType, CoderEnum.class)) {
            case GBK:
                return new StringDecoder(Charset.forName("GBK"));
            case ASCII:
                return new StringDecoder(CharsetUtil.US_ASCII);
            case HEX:
                return new HexDecoder();
            default:
                return new StringDecoder(CharsetUtil.UTF_8);
        }
    }
}
