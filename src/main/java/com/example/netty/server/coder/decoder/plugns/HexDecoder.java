package com.example.netty.server.coder.decoder.plugns;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class HexDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        byte[] data = new byte[in.readableBytes()];
        in.readBytes(data);
        StringBuilder builder = new StringBuilder();
        for (byte datum : data) {
            String str = Integer.toHexString(Byte.toUnsignedInt(datum));
            if (str.length() < 2) {
                str = "0" + str;
            }
            builder.append(str);
        }
        out.add(builder.toString());
    }
}
