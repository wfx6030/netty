package com.example.netty.server.coder.encoder.plugns;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class HexEncoder extends MessageToByteEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext ctx, String msg, ByteBuf out) throws Exception {
        msg = msg.replace("  ", "");
        byte[] dataArr = new byte[msg.length() / 2];
        for (int i = 0; i < dataArr.length; i++) {
            dataArr[i] = (byte) Integer.parseInt(msg.substring(i * 2, (i + 1) * 2), 16);
        }
        out.writeBytes(dataArr);
    }
}
