package com.example.netty.client.udp;

import com.example.netty.client.tcp.TcpClient;
import com.example.netty.model.CallBack;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UdpClientHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private final static Logger logger = LoggerFactory.getLogger(UdpClientHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        String response = msg.content().toString();
        logger.debug("通道{}——>返回值:{}", ctx.channel().id(), response);
        CallBack callBack = ctx.channel().attr(TcpClient.callBackAttributeKey).get();
        callBack.setResponse(response);
        callBack.countDown();
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }

}
